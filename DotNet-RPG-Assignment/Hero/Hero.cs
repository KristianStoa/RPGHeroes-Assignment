﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.Exceptions;
using DotNet_RPG_Assignment.HeroItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public abstract class Hero
    {
        public string Name { get; init; }
        public int Level { get; set; } = 1;
        public HeroAttribute? LevelAttribute { get; set; }
        public Dictionary<Slot, Item?>? Equipment { get; private set; }
        public List<ArmorType> ValidArmorType { get; set; }
        public List<WeaponType> ValidWeaponType { get; set; }

        public Hero(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item?>()
            {
                { Slot.Weapon, null },
                { Slot.Body, null },
                { Slot.Legs, null },
                { Slot.Head, null }
            };

        }

        public virtual void LevelUp()
        {
            Level++;
        }

        public void EquipArmor(Armor armor)
        {
            if(Level >= armor.RequiredLevel && ValidArmorType.Contains(armor.ArmorType))
            {
                Equipment[armor.Slot] = armor;
            } else
            {
                throw new InvalidArmorException("Can not equip armor :(");
            }
        }

        public void EquipWeapon(Weapon weapon)
        {
            if (Level >= weapon.RequiredLevel && ValidWeaponType.Contains(weapon.WeaponType))
            {
                Equipment[weapon.Slot] = weapon;
            } else
            {
                throw new InvalidWeaponException("Cannot equip weapon :(");
            }
            
        }
 
        public HeroAttribute totalAttribute()
        {
            HeroAttribute? totalArmorAttribute = LevelAttribute;
            foreach(var armor in Equipment)
            {
                if(armor.Value != null && armor.Key != Slot.Weapon)
                {
                    totalArmorAttribute +=((Armor)armor.Value).ArmorStats; //Usikker?
                }
            }
            return totalArmorAttribute;

        }

        public virtual double damage ()
        {
            if (Equipment[Slot.Weapon] == null)
            {
                return 1;
            }
            else
            {
                Weapon weapon = Equipment[Slot.Weapon] as Weapon;
                return weapon.WeaponDamage;
            }
        }

        public string Display()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($" Name:  {Name} ");
            stringBuilder.AppendLine($" Level:  {Level} ");
            stringBuilder.AppendLine($" Total Strength:  {totalAttribute().Strength} ");
            stringBuilder.AppendLine($" Total Dexterity:  {totalAttribute().Dexterity} ");
            stringBuilder.AppendLine($" Total Intelligence:  {totalAttribute().Intelligence} ");
            stringBuilder.AppendLine($" Damage:  {damage()} ");
            return stringBuilder.ToString();
        }

    }
}
