﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.HeroItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public class Rogue : Hero
    {
        private readonly HeroAttribute RogueStarting = new(2, 6, 1);
        private readonly HeroAttribute RogueLevelupStarting = new(1, 4, 1);

        public Rogue(string name) : base(name)
        {
            LevelAttribute = RogueStarting;

            ValidArmorType = new List<ArmorType>()
            {
                ArmorType.Leather, ArmorType.Mail
            };
            ValidWeaponType = new List<WeaponType>()
            {
                WeaponType.Daggers, WeaponType.Bows
            };
        }

        public override void LevelUp()
        {
            if (LevelAttribute == null)
                return;
            base.LevelUp();
            LevelAttribute += RogueLevelupStarting;
        }

        public override double damage()
        {
            return base.damage() * (1 + (double)totalAttribute().Dexterity / 100);
        }
    }
}
