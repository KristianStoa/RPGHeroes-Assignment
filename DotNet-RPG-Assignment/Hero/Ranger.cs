﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.HeroItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public class Ranger : Hero
    {
        private readonly HeroAttribute RangerStarting = new(1, 7, 1);
        private readonly HeroAttribute RangerLevelupStarting = new(1, 5, 1);

        public Ranger(string name) : base(name)
        {
            LevelAttribute = RangerStarting;

            ValidArmorType = new List<ArmorType>()
            {
                ArmorType.Leather, ArmorType.Mail
            };
            ValidWeaponType = new List<WeaponType>()
            {
                WeaponType.Bows
            };
        }

        public override void LevelUp()
        {
            if (LevelAttribute == null)
                return;
            base.LevelUp();
            LevelAttribute += RangerLevelupStarting;
        }

        public override double damage()
        {
            return base.damage() * (1 + (double)totalAttribute().Dexterity / 100);
        }
    }
}

