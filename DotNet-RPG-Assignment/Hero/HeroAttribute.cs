﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public static HeroAttribute operator +(HeroAttribute currentLevel, HeroAttribute levelUp) => new ( currentLevel.Strength + levelUp.Strength, currentLevel.Dexterity + levelUp.Dexterity, currentLevel.Intelligence + levelUp.Intelligence );

        public override string? ToString()
        {
            return $"Strength {Strength} Dexterity {Dexterity} Intelligence {Intelligence} ";
        }

        public override bool Equals(object? obj)
        {
            return obj is HeroAttribute attribute &&
                   Strength == attribute.Strength &&
                   Dexterity == attribute.Dexterity &&
                   Intelligence == attribute.Intelligence;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence);
        }
    }



}
