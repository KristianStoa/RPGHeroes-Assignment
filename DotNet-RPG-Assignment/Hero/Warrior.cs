﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.HeroItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public class Warrior : Hero
    {
        private readonly HeroAttribute WarriorStarting = new(5, 2, 1);
        private readonly HeroAttribute WarriorLevelupStarting = new(3, 2, 1);
        public Warrior(string name) : base(name)
        {
            LevelAttribute = WarriorStarting;

            ValidArmorType = new List<ArmorType>()
            {
                ArmorType.Mail, ArmorType.Plate
            };

            ValidWeaponType = new List<WeaponType>()
            {
                WeaponType.Axes, WeaponType.Hammers, WeaponType.Swords
            };

        }

        public override void LevelUp()
        {
            if (LevelAttribute == null)
                return;
            base.LevelUp();
            LevelAttribute += WarriorLevelupStarting;
        }

        public override double damage()
        {
            return base.damage() * (1 + (double)totalAttribute().Strength / 100);
        }

    }
}
