﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.HeroItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Hero
{
    public class Mage : Hero
    {
        private readonly HeroAttribute MageStarting = new (1, 1, 8);
        private readonly HeroAttribute MageLevelupStarting = new(1, 1, 5);
        public Mage(string name) : base(name)
        {
            LevelAttribute = MageStarting;

            ValidArmorType = new List<ArmorType>()
            {
                ArmorType.Cloth
            };

            ValidWeaponType = new List<WeaponType>() 
            { 
                WeaponType.Wands, WeaponType.Staffs 
            };

        }

        public override void LevelUp()
        {
            if (LevelAttribute == null)
                return;
            base.LevelUp();
            LevelAttribute += MageLevelupStarting;
        }

        public override double damage()
        {
            return base.damage() * (1 + (double)totalAttribute().Intelligence / 100);
        }

    }
}
