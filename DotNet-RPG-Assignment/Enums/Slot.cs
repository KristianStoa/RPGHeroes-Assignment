﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.Enums
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
