﻿using DotNet_RPG_Assignment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.HeroItem
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public double WeaponDamage { get; set; }

        public Weapon(string name, int requiredLevel, WeaponType weaponType, double weaponDamage) : base(name, requiredLevel, Slot.Weapon)
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }
    }
}
