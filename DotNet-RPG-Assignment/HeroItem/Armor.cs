﻿using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.Hero;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet_RPG_Assignment.HeroItem
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttribute ArmorStats { get; set; }

        public Armor(string name, int requiredLevel, ArmorType armorType, HeroAttribute armorStats, Slot slot) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorStats = armorStats;
        }
    }
}
