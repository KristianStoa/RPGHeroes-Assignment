﻿using DotNet_RPG_Assignment.Hero;
using DotNet_RPG_Assignment.HeroItem;
using DotNet_RPG_Assignment.Enums;

namespace DotNet_RPG_Assignment
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mage mage = new Mage("Ulrik");
            Weapon weapon = new Weapon("Staff1", 1 , WeaponType.Staffs, 2);
            Console.WriteLine(mage.LevelAttribute.ToString());
            mage.LevelUp();
            Console.WriteLine(mage.LevelAttribute.ToString());
            mage.EquipWeapon(weapon);
            Console.WriteLine(mage.damage());
            Console.WriteLine(mage.Display());
        }
    }
}