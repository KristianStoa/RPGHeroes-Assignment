using DotNet_RPG_Assignment.Hero;
using DotNet_RPG_Assignment.HeroItem;
using DotNet_RPG_Assignment.Enums;
using DotNet_RPG_Assignment.Exceptions;

namespace DotNet_RPG_AssignmentTests
{
    public class HeroTest
    {
        [Fact]
        public void Created_Hero_Has_Correct_Name()
        {
            //Arrange
            string name = "Arne Bjarne";
            Hero hero = new Mage(name);
            string expected = name;

            //Act
            string actual = hero.Name;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_Has_Correct_Level()
        {
            //Arrange 
            int level = 1;
            string name = "Arne Bjarne";
            Hero hero = new Mage(name);
            int expected = level;

            //Act
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        } 
        
        [Fact]
        public void Hero_Has_Correct_Attributes()
        {
            //Arrange 
            int intelligence = 8;
            string name = "Arne Bjarne";
            int expected = intelligence;
            Hero hero = new Mage(name);

            //Act
            int actual = hero.LevelAttribute.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_Has_Correct_LevelUp()
        {
            //Arrange 
            string name = "Knut von Trut";
            int expected = 2;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_LevelUp_Strength()
        {
            //Arrange 
            string name = "Knut von Trut";
            int expected = 8;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_LevelUp_Dexterity()
        {
            //Arrange 
            string name = "Knut von Trut";
            int expected = 4;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_LevelUp_Intelligence()
        {
            //Arrange 
            string name = "Knut von Trut";
            int expected = 2;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_Has_Correct_LevelUp()
        {
            //Arrange 
            string name = "Arne Bjarne";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_LevelUp_Strength()
        {
            //Arrange 
            string name = "Arne Bjarne";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_LevelUp_Dexterity()
        {
            //Arrange 
            string name = "Arne Bjarne";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_LevelUp_Intelligence()
        {
            //Arrange 
            string name = "Arne Bjarne";
            int expected = 13;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_Has_Correct_LevelUp()
        {
            //Arrange 
            string name = "Espen Askebakken";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_LevelUp_Strength()
        {
            //Arrange 
            string name = "Espen Askebakken";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_LevelUp_Dexterity()
        {
            //Arrange 
            string name = "Espen Askebakken";
            int expected = 12;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_LevelUp_Intelligence()
        {
            //Arrange 
            string name = "Espen Askebakken";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_Has_Correct_LevelUp()
        {
            //Arrange 
            string name = "Andreas Guttebass";
            int expected = 2;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
         [Fact]
        public void Rogue_Has_Correct_LevelUp_Strength()
        {
            //Arrange 
            string name = "Andreas Guttebass";
            int expected = 3;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }
         [Fact]
        public void Rogue_Has_Correct_LevelUp_Dexterity()
        {
            //Arrange 
            string name = "Andreas Guttebass";
            int expected = 10;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }
         [Fact]
        public void Rogue_Has_Correct_LevelUp_Intelligence()
        {
            //Arrange 
            string name = "Andreas Guttebass";
            int expected = 2;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.LevelAttribute.Intelligence;

            //Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void Weapon_Has_Correct_Name()
        {
            //Arrange 
            string name = "Daybreaker";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            double weaponDamage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);

            string expected = name;

            //Act
            string actual = weapon.Name;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_RequiredLevel()
        {
            //Arrange 
            string name = "Daybreaker";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            double weaponDamage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);

            int expected = requiredLevel;

            //Act
            int actual = weapon.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_WeaponType()
        {
            //Arrange 
            string name = "Daybreaker";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            double weaponDamage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);

            WeaponType expected = weaponType;

            //Act
            WeaponType actual = weapon.WeaponType;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_WeaponDamage()
        {
            //Arrange 
            string name = "Daybreaker";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            double weaponDamage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);

            double expected = weaponDamage;

            //Act
            double actual = weapon.WeaponDamage;

            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void Armor_Has_Correct_Name()
        {
            //Arrange 
            string name = "Shiny Mail";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, armorType, armorStats, slot);
            string expected = name;

            //Act
            string actual = armor.Name;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Armor_Has_Correct_RequiredLevel()
        {
            //Arrange 
            string name = "Shiny Mail";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, armorType, armorStats, slot);
            int expected = requiredLevel;

            //Act
            int actual = armor.RequiredLevel;

            //Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void Armor_Has_Correct_ArmorType()
        {
            //Arrange 
            string name = "Shiny Mail";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, armorType, armorStats, slot);
            ArmorType expected = armorType;

            //Act
            ArmorType actual = armor.ArmorType;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Has_Correct_ArmorStats()
        {
            //Arrange 
            string name = "Shiny Mail";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, armorType, armorStats, slot);
            HeroAttribute expected = armorStats;

            //Act
            HeroAttribute actual = armor.ArmorStats;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Armor_Has_Correct_Slot()
        {
            //Arrange 
            string name = "Shiny Mail";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, armorType, armorStats, slot);
            Slot expected = slot;

            //Act
            Slot actual = armor.Slot;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_Should_Be_Able_To_Equip_Weapon()
        {
            //Arrange
            string name = "Knut von Trut";
            string weaponName = "WabbaJack";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.Staffs;
            double weaponDamage = 5;

            Weapon weapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);
            
           
            Hero hero = new Mage(name);
            hero.EquipWeapon(weapon);


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Equal(expected[Slot.Weapon], actual[Slot.Weapon]);
        }
        [Fact]
        public void Hero_Should_Not_Be_Able_To_Equip_Weapon_ToLowLevel()
        {
            //Arrange
            string name = "Knut von Trut";
            string weaponName = "WabbaJack";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Staffs;
            double weaponDamage = 5;

            Weapon weapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);
           
            Hero hero = new Mage(name);
            


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(weapon)); 
        }
        [Fact]
        public void Hero_Should_Not_Be_Able_To_Equip_Weapon_WrongWeaponType()
        {
            //Arrange
            string name = "Knut von Trut";
            string weaponName = "WabbaJack";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.Swords;
            double weaponDamage = 5;

            Weapon weapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);
           
            Hero hero = new Mage(name);


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(weapon));

        }
        [Fact]
        public void Hero_Should_Be_Able_To_Equip_Armor()
        {
            //Arrange
            string name = "Knut von Trut";
            string armorName = "Mage Blazer";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);
            
           
            Hero hero = new Mage(name);
            hero.EquipArmor(armor);


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Equal(expected[Slot.Body], actual[Slot.Body]);
        }
        [Fact]
        public void Hero_Should_Not_Be_Able_To_Equip_Armor_ToLowLevel()
        {
            //Arrange
            string name = "Knut von Trut";
            string armorName = "Mage Blazer";
            int requiredLevel = 3;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);


            Hero hero = new Mage(name);


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(armor)); 
        }
        [Fact]
        public void Hero_Should_Not_Be_Able_To_Equip_Armor_WrongArmorType()
        {
            //Arrange
            string name = "Knut von Trut";
            string armorName = "Mage Blazer";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);


            Hero hero = new Mage(name);


            //Act
            var actual = hero.Equipment;

            //Assert
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(armor));

        }

        [Fact]
        public void TotalAttributes_Should_Be_Calculated_Correct_No_Equipent()
        {
            string name = "Knut von Trut";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            HeroAttribute expected = new(strength, dexterity, intelligence);
            Hero hero = new Mage(name);

            HeroAttribute actual = hero.totalAttribute();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_Should_Be_Calculated_Correct_One_Equipent()
        {
            string name = "Knut von Trut";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string armorName = "Mage Blazer";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;
            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);

            HeroAttribute expected = heroAttribute + armorStats;

            hero.EquipArmor(armor);

            HeroAttribute actual = hero.totalAttribute();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_Should_Be_Calculated_Correct_two_Equipent()
        {
            string name = "Knut von Trut";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string armorName = "Mage Blazer";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 0;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 1;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;
            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            string armorNameHead = "Mage Tophat";
            int requiredLevelHead = 1;
            ArmorType armorTypeHead = ArmorType.Cloth;
            int armorStrengthAttribute2 = 0;
            int armorDexterityAttribute2 = 0;
            int armorIntelligenceAttribute2 = 1;
            HeroAttribute armorStatsHead = new(armorStrengthAttribute2, armorDexterityAttribute2, armorIntelligenceAttribute2);
            Slot slot2 = Slot.Head;
            Armor armor2 = new Armor(armorNameHead, requiredLevelHead, armorTypeHead, armorStats, slot2);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);

            HeroAttribute expected = heroAttribute + armorStats + armorStatsHead;

            hero.EquipArmor(armor);
            hero.EquipArmor(armor2);

            HeroAttribute actual = hero.totalAttribute();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_Should_Be_Calculated_Correct_When_Replacing_Armor_In_Same_Slot()
        {
            string name = "Knut von Trut";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string armorName = "Mage Blazer";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Cloth;
            int armorStrengthAttribute = 0;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 1;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;
            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);

            string armorNameHead = "Mage Robe";
            int requiredLevelHead = 1;
            ArmorType armorTypeHead = ArmorType.Cloth;
            int armorStrengthAttribute2 = 0;
            int armorDexterityAttribute2 = 0;
            int armorIntelligenceAttribute2 = 1;
            HeroAttribute armorStatsHead = new(armorStrengthAttribute2, armorDexterityAttribute2, armorIntelligenceAttribute2);
            Slot slot2 = Slot.Body;
            Armor armor2 = new Armor(armorNameHead, requiredLevelHead, armorTypeHead, armorStats, slot2);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);

            HeroAttribute expected = heroAttribute + armorStatsHead;

            hero.EquipArmor(armor);
            hero.EquipArmor(armor2);

            HeroAttribute actual = hero.totalAttribute();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_Should_Be_Calculated_Correct_With_No_Weapon()
        {
            string name = "Knut von Trut";
            Hero hero = new Warrior(name);
            
            double expected = 1.05;

            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correct_With_Weapon()
        {
            string name = "Knut von Trut";
            Hero hero = new Warrior(name);

            string weaponName = "Skullcrusher";
            int requiredLevel = 1;
            WeaponType sword = WeaponType.Swords;
            int weaponDamage = 4;
            Weapon weapon = new Weapon(weaponName, requiredLevel, sword, weaponDamage);

            hero.EquipWeapon(weapon);

            double expected = 4.2;

            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correct_With_Replaced_Weapon()
        {
            string name = "Knut von Trut";
            Hero hero = new Warrior(name);

            string weaponName = "Skullcrusher";
            int requiredLevel = 1;
            WeaponType sword = WeaponType.Swords;
            int weaponDamage = 2;
            Weapon weapon = new Weapon(weaponName, requiredLevel, sword, weaponDamage);

            string weaponName2 = "WrinklePoker";
            WeaponType axe = WeaponType.Axes;
            int weaponDamage2 = 4;
            Weapon weapon2 = new Weapon(weaponName2, requiredLevel, axe, weaponDamage2);

            hero.EquipWeapon(weapon);
            hero.EquipWeapon(weapon2);

            double expected = 4.2;

            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correct_With_Weapon_And_Armor()
        {
            string name = "Knut von Trut";
            Hero hero = new Warrior(name);

            string weaponName = "Skullcrusher";
            int requiredLevel = 1;
            WeaponType sword = WeaponType.Swords;
            int weaponDamage = 2;
            Weapon weapon = new Weapon(weaponName, requiredLevel, sword, weaponDamage);

            string armorName = "Shiny Blazer";
            ArmorType armorType = ArmorType.Mail;
            int armorStrengthAttribute = 1;
            int armorDexterityAttribute = 0;
            int armorIntelligenceAttribute = 0;
            HeroAttribute armorStats = new(armorStrengthAttribute, armorDexterityAttribute, armorIntelligenceAttribute);
            Slot slot = Slot.Body;

            Armor armor = new Armor(armorName, requiredLevel, armorType, armorStats, slot);
            Dictionary<Slot, Item> equipment = new Dictionary<Slot, Item>();
            equipment.Add(Slot.Body, armor);

            hero.EquipWeapon(weapon);
            hero.EquipArmor(armor);

            double expected = 2.12;

            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
    }
}